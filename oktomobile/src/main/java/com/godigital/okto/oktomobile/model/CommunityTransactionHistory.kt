package com.godigital.okto.oktomobile.model

class CommunityTransactionHistory{
    var name = ""
    var point = ""
    var branch = ""
    var dateAndTime = ""

    constructor(name: String, point: String, branch: String, dateAndTime: String){
        this.name = name
        this.point = point
        this.branch = branch
        this.dateAndTime = dateAndTime
    }
}