package com.godigital.okto.oktomobile.errorhandling

import android.content.Context
import com.godigital.okto.oktomobile.R

/**
 * Builds an undismissable dialog for when the application is not connected to the internet.
 *
 * @author Abbey Quiñones
 * @version %I%, %G%
 * @since 0.1.0
 *
 */

class NetworkConnectivityDialog(context: Context) {

    private val dialog = ErrorDialogBuilder.buildUndismissable(context,
            context.getString(R.string.error_message_no_connection))

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        dialog.dismiss()
    }

    fun cancel() {
        dialog.cancel()
    }

    fun isShowing(): Boolean {
        return dialog.isShowing
    }
}