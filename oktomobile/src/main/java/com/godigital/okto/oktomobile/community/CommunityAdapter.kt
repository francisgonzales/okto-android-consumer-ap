package com.godigital.okto.oktomobile.community

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.databinding.CommunityBinding

class CommunityAdapter(private val context: Context, private val arrayList: ArrayList<CommunityViewModel>):RecyclerView.Adapter<CommunityAdapter.ViewHolder>(){

    lateinit var listener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        setOnItemClickListener(listener)
        val layoutInflater = LayoutInflater.from(parent.context)
        val communityBinding: CommunityBinding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.cardview_community,
                parent,
                false)

        return ViewHolder(communityBinding)
    }

    override fun getItemCount(): Int {

       return arrayList.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val communityViewModel = arrayList[position]
        holder.communityBinding.cvCommunity.setOnClickListener({
            listener.onClick(it, communityViewModel)
        })
        holder.bind(communityViewModel)
    }


    class ViewHolder(val communityBinding: CommunityBinding): RecyclerView.ViewHolder(communityBinding.root){
        fun bind (communityViewModel: CommunityViewModel){
            this.communityBinding.communitymodel = communityViewModel
            communityBinding.executePendingBindings()

        }


    }

    interface OnItemClickListener{
        fun onClick(view: View, data : CommunityViewModel)
    }

    fun setOnItemClickListener(listener: OnItemClickListener){
        this.listener = listener
    }


}