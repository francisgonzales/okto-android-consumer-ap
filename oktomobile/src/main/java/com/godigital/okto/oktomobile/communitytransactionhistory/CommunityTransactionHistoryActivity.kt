package com.godigital.okto.oktomobile.communitytransactionhistory

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.godigital.okto.oktomobile.R

class CommunityTransactionHistoryActivity : AppCompatActivity() {
    private var recyclerView:RecyclerView?=null
    private var communityTransactionHistoryAdapter: CommunityTransactionHistoryAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_community_transaction_history)

        initViews()
        initAdapter()
    }

    fun initViews(){
        recyclerView = findViewById(R.id.recyclerview_community_transaction_history)
    }

    fun initAdapter(){
        var communityTransactionHistoryViewModel: CommunityTransactionHistoryViewModel = ViewModelProviders.of(this).get(CommunityTransactionHistoryViewModel::class.java)
        communityTransactionHistoryViewModel.getArrayList().observe(this, Observer{communityTransactionHistoryViewModel->

            communityTransactionHistoryAdapter = CommunityTransactionHistoryAdapter(this@CommunityTransactionHistoryActivity, communityTransactionHistoryViewModel!!)
            recyclerView!!.setLayoutManager(LinearLayoutManager(this@CommunityTransactionHistoryActivity))
            recyclerView!!.setAdapter(communityTransactionHistoryAdapter)
        })
    }
}
