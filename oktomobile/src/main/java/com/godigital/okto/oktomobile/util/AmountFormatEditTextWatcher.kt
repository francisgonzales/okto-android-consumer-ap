package com.godigital.okto.oktomobile.util

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.errorhandling.ErrorDialogBuilder
import java.text.DecimalFormat

/**
 * Formats the EditText string into the appropriate amount format in real time.
 *
 * @author Abbey Quiñones
 * @version %I%, %G%
 * @since 0.1.0
 *
 */

class AmountFormatEditTextWatcher(private val context: Context,
                                  private val editText: EditText): TextWatcher {

    private var previousValue = ""
    private var previousSelection = 0
    private var previousDecimalPart = ""

    private val MAX_VALUE = 999999999999.99

    override fun afterTextChanged(s: Editable?) {
        editText.removeTextChangedListener(this)

        s?.let {
            val selection = editText.selectionStart

            val original = it.toString()
            val valueString = original.replace(",", "")

            // Do not process if value is empty
            if (valueString.isEmpty() || valueString == ".") return@let

            // Display error message if value exceeds maximum value
            if (valueString.toDouble() > MAX_VALUE) {
                editText.setText(previousValue)
                editText.setSelection(previousSelection)

                ErrorDialogBuilder.buildDefault(context,
                        context.getString(R.string.error_message_amount_format_max_characters))
                        .show()
                return@let
            }

            try {

                val wholeValueStr: String
                val decimalValueStr:String

                // Separate whole value part and decimal value part (include a decimal point, if applicable)
                if (valueString.contains(".")) {
                    wholeValueStr = valueString.substringBefore(".")
                    decimalValueStr = ".${valueString.substringAfter(".")}"
                } else {
                    wholeValueStr = valueString
                    decimalValueStr = ""
                }

                // Format whole value part
                val wholeValuePart =
                    if (wholeValueStr.isNotEmpty()) {
                        val value = wholeValueStr.toLong()
                        val formatter = DecimalFormat("#,###")
                        formatter.format(value)
                    } else ""

                // Format decimal value part. Ensure decimal only has two places
                val decimalValuePart =
                    if (decimalValueStr.isNotEmpty()) {
                        if (decimalValueStr.length > 3) {
                            ErrorDialogBuilder.buildDefault(context,
                                    context.getString(R.string.error_message_amount_format_max_decimal_characters))
                                    .show()
                            previousDecimalPart
                        }
                        else decimalValueStr
                    } else ""

                previousDecimalPart = decimalValuePart

                val newString = wholeValuePart + decimalValuePart

                // Compute cursor position
                val selectionDiff = original.length - newString.length
                var newSelection = selection - selectionDiff
                if (newSelection < 0) newSelection = 0
                if (newSelection > newString.length) newSelection = newString.length

                editText.setText(newString)
                editText.setSelection(newSelection)

            } catch (e: NumberFormatException) {
                e.printStackTrace()
                editText.setText(previousValue)
                editText.setSelection(previousSelection)

                ErrorDialogBuilder.buildDefault(context,
                        context.getString(R.string.error_message_amount_format_invalid_number))
                        .show()
            }

        }

        editText.addTextChangedListener(this)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        previousSelection = editText.selectionStart
        previousValue = editText.text.toString()
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

}