package com.godigital.okto.oktomobile.communitytransactionhistory


import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.databinding.CommunityTransactionHistoryBinding

class CommunityTransactionHistoryAdapter(private val context : Context, private val arrayList: ArrayList<CommunityTransactionHistoryViewModel>): RecyclerView.Adapter<CommunityTransactionHistoryAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val communityTransactionHistoryBinding: CommunityTransactionHistoryBinding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.cardview_community_transaction_history,
                parent,
                false)

        return ViewHolder(communityTransactionHistoryBinding)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        val communityTransactionHistoryViewModel = arrayList[position]
        holder.bind(communityTransactionHistoryViewModel)
    }

    class ViewHolder(val communityTransactionHistoryBinding: CommunityTransactionHistoryBinding): RecyclerView.ViewHolder(communityTransactionHistoryBinding.root){
        fun bind(communityTransactionHistoryViewModel: CommunityTransactionHistoryViewModel){
            this.communityTransactionHistoryBinding.communitytransactionhistorymodel = communityTransactionHistoryViewModel
            communityTransactionHistoryBinding.executePendingBindings()
        }
    }
}