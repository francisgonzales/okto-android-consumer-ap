package com.godigital.okto.oktomobile.util

/**
 * Holds the string keys for intent extras.
 *
 * @author Abbey Quiñones
 * @version %I%, %G%
 * @since 0.1.0
 *
 */

class IntentExtras {

    object AllLoadProvidersActivity {
        const val SELECTED_LOAD_PROVIDER = "AllLoadProvidersActivity.selectedLoadProvider"
    }

    object AllBillersActivity {
        const val BILLER_ID = "AllBillersActivity.billerId"
    }

    object BuyLoadActivity {
        const val LOAD_TRANSACTION_ID = "BuyLoadActivity.transactionId"
        const val LOAD_DENOMINATION = "BuyLoadActivity.loadPromo"
        const val LOAD_RECIPIENT = "BuyLoadActivity.recipient"
        const val LOAD_AMOUNT = "BuyLoadActivity.amount"
        const val LOAD_DATE = "BuyLoadActivity.date"
        const val LOAD_TIME = "BuyLoadActivity.time"
    }

    object PayBillsActivity {
        const val BILL_TRANSACTION_ID = "PayBillsActivity.transactionId"
        const val BILL_COMPANY = "PayBillsActivity.company"
        const val BILL_ACCOUNT_NUMBER = "PayBillsActivity.accountNumber"
        const val BILL_AMOUNT = "PayBillsActivity.amount"
        const val BILL_DATE = "PayBillsActivity.date"
        const val BILL_TIME = "PayBillsActivity.time"
        const val BILLER_ID = "PayBillsActivity.billerId"
    }

    object SendMoneyActivity {
        const val TRANSACTION_ID = "SendMoneyActivity.transactionId"
        const val TRANSACTION_DATE = "SendMoneyActivity.date"
        const val TRANSACTION_TIME = "SendMoneyActivity.time"
        const val TRANSACTION_RECIPIENT = "SendMoneyActivity.recipient"
        const val TRANSACTION_AMOUNT = "SendMoneyActivity.amount"
        const val TRANSACTION_REMARKS = "SendMoneyActivity.remarks"
    }

    object ScanToPayActivity {
        const val SCANNER_OUTPUT = "ScanToPayActivity.scannerOutput"
        const val TRANSACTION_ID = "ScanToPayActivity.transactionId"
        const val TRANSACTION_DATE = "ScanToPayActivity.date"
        const val TRANSACTION_TIME = "ScanToPayActivity.time"
        const val TRANSACTION_RECIPIENT = "ScanToPayActivity.recipient"
        const val TRANSACTION_AMOUNT = "ScanToPayActivity.amount"
        const val TRANSACTION_REMARKS = "ScanToPayActivity.remarks"
    }

    object CreateAccountActivity {
        const val TERMS_AGREEMENT = "CreateAccountActivity.termsAgreement"
    }

}