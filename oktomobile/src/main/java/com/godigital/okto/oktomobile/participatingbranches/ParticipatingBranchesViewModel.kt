package com.godigital.okto.oktomobile.participatingbranches

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.widget.ImageView
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.model.ParticipatingBranches
import com.squareup.picasso.Picasso

class ParticipatingBranchesViewModel : ViewModel{
    var name = ""
    var thumbnail = ""


    constructor() : super()

    constructor(participatingBranches: ParticipatingBranches) : super() {
        this.name = participatingBranches.name
        this.thumbnail = participatingBranches.thumbnail
    }

   fun onClick() {
       //ParticipatingBranchesActivity().thisClick()
   }


    var arrayListMutableLiveData = MutableLiveData<ArrayList<ParticipatingBranchesViewModel>>()
    var arrayList = ArrayList<ParticipatingBranchesViewModel>()

    fun getImageUrl():String{
        return thumbnail
    }

    fun getArrayList():MutableLiveData<ArrayList<ParticipatingBranchesViewModel>>{
        val participatingBranches1 = ParticipatingBranches("White Wolf(D. Tuazon)","ic_coin_gold.png")
        val participatingBranches2 = ParticipatingBranches("White Wolf(Katipunan)","ic_coin_silver.png")
        val participatingBranches3 = ParticipatingBranches("White Wolf(Taft)","ic_coin_gold.png")
        val participatingBranches4 = ParticipatingBranches("White Wolf(Tomas Morato)","ic_coin_silver.png")
        val participatingBranches5 = ParticipatingBranches("White Wolf(Malolos)","ic_coin_gold.png")
        val participatingBranches6 = ParticipatingBranches("White Wolf(Aurora)","ic_coin_silver.png")



        val participatingBranchesViewModel1 : ParticipatingBranchesViewModel = ParticipatingBranchesViewModel(participatingBranches1)
        val participatingBranchesViewModel2 : ParticipatingBranchesViewModel = ParticipatingBranchesViewModel(participatingBranches2)
        val participatingBranchesViewModel3 : ParticipatingBranchesViewModel = ParticipatingBranchesViewModel(participatingBranches3)
        val participatingBranchesViewModel4 : ParticipatingBranchesViewModel = ParticipatingBranchesViewModel(participatingBranches4)
        val participatingBranchesViewModel5 : ParticipatingBranchesViewModel = ParticipatingBranchesViewModel(participatingBranches5)
        val participatingBranchesViewModel6 : ParticipatingBranchesViewModel = ParticipatingBranchesViewModel(participatingBranches6)

        arrayList!!.add(participatingBranchesViewModel1)
        arrayList!!.add(participatingBranchesViewModel2)
        arrayList!!.add(participatingBranchesViewModel3)
        arrayList!!.add(participatingBranchesViewModel4)
        arrayList!!.add(participatingBranchesViewModel5)
        arrayList!!.add(participatingBranchesViewModel6)


        arrayListMutableLiveData.value = arrayList

        return  arrayListMutableLiveData
    }

}


    @BindingAdapter("android:src")
    fun setImageUrl(view: ImageView, url:String){
        Picasso.with(view.context).load(url).placeholder(R.drawable.community_thumbnail_3).into(view)
    }
