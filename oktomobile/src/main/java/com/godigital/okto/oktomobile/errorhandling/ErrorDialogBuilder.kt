package com.godigital.okto.oktomobile.errorhandling

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import com.godigital.okto.oktomobile.R

/**
 * Builds the default error dialog for the application.
 *
 * @author Abbey Quiñones
 * @version %I%, %G%
 * @since 0.1.0
 *
 */

object ErrorDialogBuilder {

    /**
     * Builds a standard dialog without buttons.
     */
    fun build(context: Context, title: String, message: String, isCancellable: Boolean): AlertDialog {
        val builder = AlertDialog.Builder(context, R.style.Widget_Okto_AlertDialog)
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(isCancellable)

        return builder.create()
    }

    /**
     * Builds a standard dialog with one button.
     */
    fun build(context: Context, title: String, message: String, isCancellable: Boolean,
              buttonText: String, onButtonClick: (dialog: DialogInterface) -> Unit): AlertDialog {
        val builder = AlertDialog.Builder(context, R.style.Widget_Okto_AlertDialog)
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(isCancellable)
                .setPositiveButton(buttonText) { dialog, _ -> onButtonClick(dialog) }

        return builder.create()
    }

    /**
     * Builds a standard dismissable error dialog with an OK button.
     */
    fun buildDefault(context: Context, message: String): AlertDialog {
        val title = context.getString(R.string.error_dialog_title_default)
        val buttonText = context.getString(R.string.button_ok)

        return build(context, title, message, true, buttonText) {}
    }

    /**
     * Builds a standard dismissable error dialog with a Retry button.
     */
    fun buildRetry(context: Context, message: String,
                  onRetry: (dialog: DialogInterface) -> Unit): AlertDialog {
        val title = context.getString(R.string.error_dialog_title_default)
        val buttonText = context.getString(R.string.button_retry)

        return build(context, title, message, true, buttonText, onRetry)
    }

    /**
     * Builds an undismissable error dialog with no buttons.
     */
    fun buildUndismissable(context: Context, message: String): AlertDialog {
        val title = context.getString(R.string.error_dialog_title_default)

        return build(context, title, message, false)
    }

}