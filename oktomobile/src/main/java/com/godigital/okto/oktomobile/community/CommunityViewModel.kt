package com.godigital.okto.oktomobile.community

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.widget.ImageView
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.model.Community
import com.squareup.picasso.Picasso

class CommunityViewModel : ViewModel {
    var name = ""
    var point = ""
    var thumbnail = ""
    var coinImage = ""

    constructor() : super()

    constructor(community: Community) : super() {
        this.name = community.name
        this.point = community.point
        this.thumbnail = community.thumbnail
        this.coinImage = community.coinImage
    }

    var arrayListMutableLiveData = MutableLiveData<ArrayList<CommunityViewModel>>()
    var arrayList = ArrayList<CommunityViewModel>()

    fun getImageUrl():String{
        return thumbnail
    }

    fun getArrayList():MutableLiveData<ArrayList<CommunityViewModel>>{
        val community1 = Community(
                "White Wolf",
                "99 999",
                "community_thumbnail_3.png",
                "ic_coin_gold.png")
        val community2 = Community(
                "Oppa Chikin",
                "99 999",
                "community_thumbnail_1.png",
                "ic_coin_silver.png")

        val communityViewModel1 : CommunityViewModel = CommunityViewModel(community1)
        val communityViewModel2 : CommunityViewModel = CommunityViewModel(community2)

        arrayList!!.add(communityViewModel1)
        arrayList!!.add(communityViewModel2)

        arrayListMutableLiveData.value = arrayList

        return  arrayListMutableLiveData
    }

}

object ImageBindingAdapte{
    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageUrl(view: ImageView, url:String){
        Picasso.with(view.context).load(url).placeholder(R.drawable.community_thumbnail_3).into(view)
    }
}