package com.godigital.okto.oktomobile.communitypage

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.view.View
import android.widget.ImageView
import com.godigital.okto.oktomobile.BR
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.model.CommunityPage
import com.squareup.picasso.Picasso
import java.util.*

class CommunityPageViewModel(private val communityPage: CommunityPage,val listener : Listener): Observer, BaseObservable() {

//    private var eventHandler: EventHandler

    init {
//        eventHandler = EventHandler(listener)
        communityPage.addObserver(this)
    }
    override fun update(o: Observable?, cp: Any?) {
        if(cp is String){
            if (cp == "banner"){
                notifyPropertyChanged(BR.banner)
            }else if(cp == "coinImage"){
                notifyPropertyChanged(BR.coinImage)
            }else if(cp == "point"){
                notifyPropertyChanged(BR.point)
            } else if(cp == "description"){
                notifyPropertyChanged(BR.description)
            }
        }

    }



    val banner: String
        @Bindable get() {
            return communityPage.banner

        }

    val coinImage: String
        @Bindable get() {
            return communityPage.coinImage
        }

    val point: String
        @Bindable get() {
            return communityPage.point
        }

    val description: String
        @Bindable get() {
            return communityPage.description
        }


    //Need to fix image loaders
    @BindingAdapter( "bind:banner")
    fun loadImageBanner(view: ImageView, url: String){
        Picasso.with(view.context).load(url).placeholder(R.drawable.dummy_promo1).into(view)
    }


    @BindingAdapter( "bind:coinImage")
    fun loadImageCoin(view: ImageView, url: String){
        Picasso.with(view.context).load(url).placeholder(R.drawable.ic_coin_gold).into(view)
    }

    interface Listener{
        fun transactionHistoryButtonClicked()
        fun participatingBranchesButtonClicked()
    }

    @Suppress("UNUSED_PARAMETER")
    fun transactionHistoryButtonClicked(view: View){
        listener.transactionHistoryButtonClicked()
    }

    @Suppress("UNUSED_PARAMETER")
    fun participatingBranchesButtonClicked(view: View){
        listener.participatingBranchesButtonClicked()
    }

}