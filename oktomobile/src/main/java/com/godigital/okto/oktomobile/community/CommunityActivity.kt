package com.godigital.okto.oktomobile

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.View
import com.godigital.okto.oktomobile.communitypage.CommunityPageActivity
import com.godigital.okto.oktomobile.community.CommunityAdapter
import com.godigital.okto.oktomobile.community.CommunityViewModel
import kotlinx.android.synthetic.main.activity_community.*

class CommunityActivity : AppCompatActivity() {
    private var recyclerView:RecyclerView?=null
    private var communityAdapter: CommunityAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_community)
        setSupportActionBar(toolbar_community)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initViews()
        initCommunityAdapter()
    }

    fun initViews() {
        recyclerView = findViewById(R.id.recyclerview_community)
    }

    fun initCommunityAdapter() {
        var communityViewModel: CommunityViewModel = ViewModelProviders.of(this).get(CommunityViewModel::class.java)
        communityViewModel.getArrayList().observe(this, Observer {communityViewModel->

            communityAdapter = CommunityAdapter(this@CommunityActivity, communityViewModel!!)
            recyclerView!!.setLayoutManager(LinearLayoutManager(this@CommunityActivity))
            recyclerView!!.setAdapter(communityAdapter)
            communityAdapter!!.setOnItemClickListener(object : CommunityAdapter.OnItemClickListener{
                override fun onClick(view: View, data: CommunityViewModel) {
                    val intent = Intent(applicationContext, CommunityPageActivity::class.java)
                    startActivity(intent)
                }
            })

        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
                menuInflater.inflate(R.menu.menu_community, menu)
                return true
           }

}