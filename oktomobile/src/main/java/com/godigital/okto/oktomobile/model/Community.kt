package com.godigital.okto.oktomobile.model


class Community{
    var name = ""
    var point = ""
    var thumbnail = ""
    var coinImage = ""

    constructor(name: String, point: String, image_path: String, coin: String) {
        this.name = name
        this.point = point
        this.thumbnail = image_path
        this.coinImage = coin
    }
}