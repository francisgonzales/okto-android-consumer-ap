package com.godigital.okto.oktomobile.model

import java.util.*

class User: Observable() {

    var avatar : String = ""
        set(value) {
            field = value
            setChangedAndNotify("avatar")
        }

    var name : String = ""
        set(value) {
            field = value
            setChangedAndNotify("name")
        }

    var registeredDate : String = ""
        set(value) {
            field = value
            setChangedAndNotify("registeredDate")
        }

    fun setChangedAndNotify(field : Any){
        setChanged()
        notifyObservers(field)
    }


}