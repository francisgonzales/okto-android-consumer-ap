package com.godigital.okto.oktomobile.participatingbranches

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.databinding.ParticipatingBranchesBinding

class ParticipatingBranchesAdapter(private val context: Context, private val arrayList: ArrayList<ParticipatingBranchesViewModel>): RecyclerView.Adapter<ParticipatingBranchesAdapter.CustomView>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {

        val layoutInflater = LayoutInflater.from(parent.context)
        val participatingBranchesBinding: ParticipatingBranchesBinding = DataBindingUtil.inflate(layoutInflater, R.layout.cardview_participating,parent,false)
        return CustomView(participatingBranchesBinding)
    }

    override fun getItemCount(): Int {

        return arrayList.size
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {


        val participatingBranchesViewModel = arrayList[position]
        holder.bind(participatingBranchesViewModel)
    }


    class CustomView(val participatingBranchesBinding: ParticipatingBranchesBinding): RecyclerView.ViewHolder(participatingBranchesBinding.root){
        fun bind (participatingBranchesViewModel: ParticipatingBranchesViewModel){
            this.participatingBranchesBinding.participatingBranchesmodel = participatingBranchesViewModel
            participatingBranchesBinding.executePendingBindings()
        }
    }
}