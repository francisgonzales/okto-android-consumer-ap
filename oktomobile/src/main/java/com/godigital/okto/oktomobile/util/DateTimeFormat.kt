package com.godigital.okto.oktomobile.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * Holds pre-defined SimpleDateFormat constants used for formatting Date objects.
 *
 * @author Abbey Quiñones
 * @version %I%, %G%
 * @since 0.1.0
 *
 */

object DateTimeFormat {

    private val locale: Locale = Locale.getDefault()
    val DATE_TIME = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", locale)
    val DATE_ONLY = SimpleDateFormat("MM/dd/yyyy", locale)
    val DATE_ONLY_SPELLED = SimpleDateFormat("MMM dd, yyyy", locale)
    val TIME_ONLY = SimpleDateFormat("hh:mm a", locale)

}