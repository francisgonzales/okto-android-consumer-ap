package com.godigital.okto.oktomobile.model


class ParticipatingBranches{
    var name = ""
    var thumbnail = ""


    constructor(name: String, image_path: String) {
        this.name = name
        this.thumbnail = image_path

    }
}