package com.godigital.okto.oktomobile.activity

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import com.godigital.okto.oktomobile.BR
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.model.CommunityPage
import com.godigital.okto.oktomobile.viewmodel.CommunityPageViewModel
import kotlinx.android.synthetic.main.activity_community.*
import kotlinx.android.synthetic.main.activity_community_page.*

class CommunityPageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_community_page)
//        setSupportActionBar(toolbar_community_page)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val communityPage = CommunityPage()

        communityPage.banner = "dummy_promo1.png"
        communityPage.point = "99 999"
        communityPage.description = "Lorem ipsum dolor sit amet, non nunc mollis fringilla duis molestie cursus, " +
                "porttitor felis lobortis sagittis sed donec, eu semper mauris, dui varius ut sed vitae eget. " +
                "Lacus ut curabitur turpis."
        communityPage.coinImage = "ic_coin_gold.png"

        val communityPageViewModel = CommunityPageViewModel(communityPage,object:CommunityPageViewModel.Listener{
            override fun transactionHistoryButtonClicked() {
            }

            override fun participatingBranchesButtonClicked() {
            }

        })
        val binding = DataBindingUtil.setContentView<ViewDataBinding>(this,R.layout.activity_community_page)
        binding.setVariable(BR.communitypagemodel,communityPageViewModel)

    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//                menuInflater.inflate(R.menu.menu_community, menu)
//                return true
//            }
}
