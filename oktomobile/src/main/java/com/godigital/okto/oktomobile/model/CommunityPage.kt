package com.godigital.okto.oktomobile.model

import java.util.*

class CommunityPage: Observable(){

    var banner : String = ""
        set(value) {
            field = value
            setChangedAndNotify("banner")
        }

    var coinImage : String = ""
        set(value) {
            field = value
            setChangedAndNotify("coinImage")
        }

    var point : String = ""
        set(value) {
            field = value
            setChangedAndNotify("point")
        }

    var description : String = ""
        set(value) {
            field = value
            setChangedAndNotify("description")
        }


    fun setChangedAndNotify(field : Any){
        setChanged()
        notifyObservers(field)
    }
}