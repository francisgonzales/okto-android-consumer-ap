package com.godigital.okto.oktomobile.communitytransactionhistory

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.godigital.okto.oktomobile.model.CommunityTransactionHistory

class CommunityTransactionHistoryViewModel: ViewModel{
    var name = ""
    var point = ""
    var branch = ""
    var dateAndTime = ""

    constructor(): super()

    constructor(communityTransactionHistory: CommunityTransactionHistory): super(){
        this.name = communityTransactionHistory.name
        this.point = communityTransactionHistory.point
        this.branch = communityTransactionHistory.branch
        this.dateAndTime = communityTransactionHistory.dateAndTime
    }

    var arrayListMutableLiveData = MutableLiveData<ArrayList<CommunityTransactionHistoryViewModel>>()
    var arrayList = ArrayList<CommunityTransactionHistoryViewModel>()

    fun getArrayList(): MutableLiveData<ArrayList<CommunityTransactionHistoryViewModel>> {
        val cth1 = CommunityTransactionHistory(
                "White Wolf",
                "2",
                "White Wolf(D Tuazon)",
                "07:00 AM - 01/15/2018"
        )

        val cth2 = CommunityTransactionHistory(
                "White Wolf",
                "8",
                "White Wolf(Katipunan)",
                "07:00 AM - 01/14/2018"
        )

        val cth3 = CommunityTransactionHistory(
                "White Wolf",
                "4",
                "White Wolf(Aurora)",
                "07:00 AM - 01/13/2018"
        )

        val cth4 = CommunityTransactionHistory(
                "White Wolf",
                "10",
                "White Wolf(Regalado)",
                "07:00 AM - 01/12/2018"
        )

        val cth5 = CommunityTransactionHistory(
                "White Wolf",
                "2",
                "White Wolf(Tomas Morato)",
                "07:00 AM - 01/11/2018"
        )


        val cthvm1 : CommunityTransactionHistoryViewModel = CommunityTransactionHistoryViewModel(cth1)
        val cthvm2 : CommunityTransactionHistoryViewModel = CommunityTransactionHistoryViewModel(cth2)
        val cthvm3 : CommunityTransactionHistoryViewModel = CommunityTransactionHistoryViewModel(cth3)
        val cthvm4 : CommunityTransactionHistoryViewModel = CommunityTransactionHistoryViewModel(cth4)
        val cthvm5 : CommunityTransactionHistoryViewModel = CommunityTransactionHistoryViewModel(cth5)

        arrayList!!.add(cthvm1)
        arrayList!!.add(cthvm2)
        arrayList!!.add(cthvm3)
        arrayList!!.add(cthvm4)
        arrayList!!.add(cthvm5)

        arrayListMutableLiveData.value = arrayList

        return arrayListMutableLiveData
    }
}

