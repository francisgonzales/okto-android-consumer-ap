package com.godigital.okto.oktomobile.util

import java.text.DecimalFormat

/**
 * Holds pre-defined DecimalFormat objects for number values used in the application.
 *
 * @author Abbey Quiñones
 * @version %I%, %G%
 * @since 0.1.0
 *
 */

object NumberFormat {
    val BALANCE_FORMAT = DecimalFormat("#,##0.00")
    val POINTS_FORMAT = DecimalFormat("#,##0.00")

    val YEAR_FORMAT = DecimalFormat("###0")
    val MONTH_FORMAT = DecimalFormat("00")
    val DAY_FORMAT = DecimalFormat("00")
}