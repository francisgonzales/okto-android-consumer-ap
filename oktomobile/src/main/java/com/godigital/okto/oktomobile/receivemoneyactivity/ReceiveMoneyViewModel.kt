package com.godigital.okto.oktomobile.receivemoneyactivity

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.widget.ImageView
import com.godigital.okto.oktomobile.BR
import com.godigital.okto.oktomobile.model.User
import com.squareup.picasso.Picasso
import java.util.*

class ReceiveMoneyViewModel(private val user:User): Observer, BaseObservable(){

    init {
        user.addObserver(this)
    }

    override fun update(o: Observable?, u: Any?) {
        if(u is String){
            if (u == "name"){
                notifyPropertyChanged(BR.name)
            }else if(u == "avatar"){
                notifyPropertyChanged(BR.avatar)
            }else if(u == "registeredDate"){
                notifyPropertyChanged(BR.registeredDate)
            }
        }
    }


    val name: String
        @Bindable get() {
            return user.name

        }

    val avatar: String
        @Bindable get() {
            return user.avatar
        }

    val registeredDate: String
        @Bindable get() {
            return user.registeredDate
        }



//    //Need to fix image loaders
//    @BindingAdapter( "bind:avatar")
//    fun loadImageBanner(view: ImageView, url: String){
//        Picasso.with(view.context).load(url).placeholder(R.drawable.ic_ava_gray).into(view)
//    }

}
