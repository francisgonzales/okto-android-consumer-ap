package com.godigital.okto.oktomobile.participatingbranches

import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import com.godigital.okto.oktomobile.R
import android.os.CountDownTimer



class ParticipatingBranchesActivity(val activity: Activity) : AppCompatActivity(){
    private var recyclerView:RecyclerView?=null
    private var participatingBranchesAdapter: ParticipatingBranchesAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_participating_branches)

        recyclerView = findViewById(R.id.recyclerview_participating_branches)


        object : CountDownTimer(5000, 1000) {

            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
            thisClick()
            }

        }.start()




        var participatingBranchesViewModel: ParticipatingBranchesViewModel = ViewModelProviders.of(this).get(ParticipatingBranchesViewModel::class.java)
        participatingBranchesViewModel.getArrayList().observe(this, Observer { participatingBranchesViewModel->

            participatingBranchesAdapter = ParticipatingBranchesAdapter(this@ParticipatingBranchesActivity, participatingBranchesViewModel!!)
            recyclerView!!.setLayoutManager(LinearLayoutManager(this@ParticipatingBranchesActivity))
            recyclerView!!.setAdapter(participatingBranchesAdapter)

        })
    }



    fun thisClick() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.setContentView(R.layout.dialog_branch_information)
        val window = dialog.window
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.BOTTOM)
        dialog.show()

    }
}