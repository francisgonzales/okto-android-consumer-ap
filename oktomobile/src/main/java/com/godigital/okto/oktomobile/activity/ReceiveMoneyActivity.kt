package com.godigital.okto.oktomobile.activity

import android.databinding.DataBindingUtil
import android.databinding.DataBindingUtil.setContentView
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.godigital.okto.oktomobile.BR
import com.godigital.okto.oktomobile.R
import com.godigital.okto.oktomobile.model.User
import com.godigital.okto.oktomobile.viewmodel.ReceiveMoneyViewModel

class ReceiveMoneyActivity: AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recieve_money)

        val user = User()
        user.name = "Juan Change Dela Cruz"
        user.avatar = "ic_ava_gray.png"
        user.registeredDate = "Verified Member since May 2015"


        val receiveMoneyViewModel = ReceiveMoneyViewModel(user)
        val binding = DataBindingUtil.setContentView<ViewDataBinding>(this,R.layout.activity_recieve_money)
        binding.setVariable(BR.receivemoneymodel,receiveMoneyViewModel)

    }
}