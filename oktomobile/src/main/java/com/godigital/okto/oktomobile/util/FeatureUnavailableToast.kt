package com.godigital.okto.oktomobile.util

import android.content.Context
import android.widget.Toast

/**
 * Builds and shows a toast when a feature tapped is not available.
 *
 * @author Abbey Quiñones
 * @version %I%, %G%
 * @since 0.1.0
 *
 */

class FeatureUnavailableToast constructor(val context: Context) {

    fun show() {
        Toast.makeText(context,
                "This feature is not yet available.",
                Toast.LENGTH_SHORT).show()
    }

}